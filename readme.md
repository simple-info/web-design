#網站設計流程
## 預先定義
1. 內容
2. 功能
3. 頁面(sitemap)
4. 各頁面內容功能

## 設計

1. 版型 (layout)
    1. Grid System Design
        1. columns
        2. gutter (水溝) 
    2. Responsive Web Design
    3. Fix Area
    4. Special Component(alert modal back-to-top)
    5. 每個區域的功能 (logo-area、user-panel、menu.......)
2. 色彩
    1. 色票
        1. 色彩
        2. 透明度 
    2. 漸層
    3. 使用情境 
3. 各頁面排版
    1. 使用元件
    2. 商業邏輯
        1. 特色圖片規格
        2. 字數 (標題、文摘)
        3. 其他(日期格式)
    3. 對應的資料格式 
4. 元件
    1. 文字 
        1. 字型(大小 字體 寬度 行距)
        2. 背景色
        3. overflow 顯示模式(加上...、直接隱藏、scroll)
        4. 特殊文字
            1. 超連結
            2. 標題(h1-5) 
    2. 表格
        1. 框線
        2. 留白
    3. 表單
        1. 類型
            1. select (下拉式單選)
            2. multi-select (多選)
            3. radio
            4. checkbox
            5. text
                1. placeholder(還沒打字的時候出現的東東)
        2. 共通元素
            1. 顏色
            2. outline 
        3. 驗證狀態
            1. 驗證成功 (success)
            2. 驗證失敗 (danger)
            3. 警告 (notice)
    4. 圖片
        1. 外框
        2. 圓角
5. 組合元件
    1. 分頁
    2. 按鈕、按鈕群組
    3. 選單、子選單
    4. 表格
    5. 可參考 Bootstrap、materialize design 的元件
6. 網站動態
    1. 版型的動畫
        1. 事件導向(例如：進入頁面、離開頁面頂端或特定高度、滑到頁底)
        2. 轉場
    2. 微動畫(hover、focus、typeing......)
    3. lazy-load(文章、圖片讀取前後)
    4. 影片
    5. canvas animation
    6. 游標偵測
        
## 參考資料
1. [動畫可用元素](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_animated_properties) 
2. [Bootstrap 自訂設計](http://getbootstrap.com/customize/)
3. [Materialize Design](http://materializecss.com/about.html)
4. [蔡英文競選官網設計範例](http://design.iing.tw/)
